# lude

this is meant to be a dumping ground for utility functions i use regularly in real world haskell applications.

it's pretty much my supplement to RIO.

it started out as a fork of [magicbane](https://hackage.haskell.org/package/magicbane), but whittled down to this once i started pruning the stuff i didn't think i would get much milage from outside of a web-app.

## License

This is free and unencumbered software released into the public domain.  
For more information, please refer to the `UNLICENSE` file or [unlicense.org](https://unlicense.org).

(However, the dependencies are not all unlicense'd!)
