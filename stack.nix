{ ghc }:
let
  pkgs =
    import ./nixpkgs.nix { };
in
  with pkgs; haskell.lib.buildStackProject {
    inherit ghc;
    name = "lude";
    buildInputs = [ zlib git ];
  }
