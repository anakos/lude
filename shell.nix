let
  pinnedVersion =
    builtins.fromJSON (builtins.readFile ./.nixpkgs-version.json);
  pinnedPkgs =
    import (builtins.fetchGit {
      inherit (pinnedVersion) url rev;
      ref = "nixos-unstable";
    }) { };

  haskell865 =
    pinnedPkgs.haskell.packages.ghc865.ghcWithHoogle (
          haskellPackages: with haskellPackages; [
            aeson
            array
            arrows
            async
            binary
            bytestring
            conduit
            conduit-extra
            criterion
            http-conduit
            xml-conduit
            containers
            criterion
            deepseq
            hedgehog
            hspec
            hspec-core
            lens
            lifted-async
            mtl
            optparse-applicative
            QuickCheck
            random
            rio
            req
            resourcet
            stm
            tasty
            tasty-hedgehog
            text
            turtle
            warp
            # tools
            cabal-install
            alex
            doctest
            ghcid
            happy
            happy
            hindent
            hlint
            stack
            stylish-haskell
            hasktags
            apply-refact
            cabal-install
          ]);
  
in

# This allows overriding pkgs by passing `--arg pkgs ...`
{ pkgs ? pinnedPkgs }:

with pkgs; mkShell {
  SSL_CERT_DIR  = "/etc/ssl/certs";
  SSL_CERT_FILE = "/etc/ssl/certs/ca-certificates.crt";
  nativeBuildInputs = [
    # environment
    binutils
    gcc
    gnumake
    openssl
    pkgconfig
    git
    cacert
  ];
  
  buildInputs = [
    # environment
    binutils
    gcc
    gnumake
    openssl
    pkgconfig
    git
    gnuplot
    wrk2
    apacheHttpd
    # haskell
    haskell865
  ] ++
    pkgs.stdenv.lib.optionals pkgs.stdenv.isDarwin [
      pkgs.darwin.cf-private
      pkgs.darwin.Security
      pkgs.darwin.apple_sdk.frameworks.CoreServices
  ];
  shellHook = ''
  ''; 
}
