{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Trustworthy       #-}

-- | This is my prelude for everyday development, which for now is RIO + Lens + Metrics + Logging
--
module Lude (
  module X
, module Lude
) where

import           Data.String.Conversions             as X hiding ((<>))
import           Data.String.Conversions.Monomorphic as X hiding (fromString)
import           Lude.Has                            as X
import           Lude.Logging                        as X
import           Lude.Metrics                        as X
import           Safe                                as X hiding (at)
-- TODO: add gauges, counters, label
import           RIO                                 (Display (..), MonadIO,
                                                      Utf8Builder (..),
                                                      hPutBuilder, stdout)
import           System.Remote.Monitoring            as X
import           Text.RawString.QQ                   as X
displayLn
  :: (MonadIO m, Display a)
  => a
  -> m ()
displayLn =
  hPutBuilder stdout . getUtf8Builder . (<> "\n") . display
{-# INLINE displayLn #-}

