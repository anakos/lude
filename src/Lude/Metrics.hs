{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE RankNTypes           #-}
{-# LANGUAGE UndecidableInstances #-}

-- | Provides metrics via monad-metrics/EKG.
--   Also reexports Wai metrics middleware.
module Lude.Metrics (
  module Lude.Metrics
, module X
) where

import           Control.Monad.Metrics as X hiding (initialize, initializeWith,
                                             run, run')
import qualified Control.Monad.Metrics
import           Data.Has              (Has)
import           Lude.Has              (askOpt)
import           RIO
import           System.Metrics        as X

newtype ModMetrics = ModMetrics Metrics

-- | Creates a metrics module with a particular Store.
--   The Store should come from the backend you want to use for storing the metrics.
newMetricsWith
  :: Store
  -> IO ModMetrics
newMetricsWith =
  fmap ModMetrics . Control.Monad.Metrics.initializeWith

instance (Has ModMetrics α, Monad μ, MonadReader α μ) => X.MonadMetrics μ where
  getMetrics = askOpt $! \(ModMetrics m) -> m
