{-# LANGUAGE Trustworthy #-}

module Lude.Has (
  module X
, module Lude.Has
) where

import           Data.Has as X
import           RIO

-- | Gets a value β from α in μ.
askObj
  :: (Has β α, MonadReader α μ)
  => μ β
askObj =
  asks getter

-- | Map a value from β to ψ, provided by α in μ.
askOpt
  :: (Has β α, MonadReader α μ)
  => (β -> ψ)
  -> μ ψ
askOpt f = asks $ f . getter
